#!/bin/bash
# Switch keyboard layout between ru and eng on super + space
# author: syr1ken

setxkbmap -layout 'us,ru' -option 'grp:win_space_toggle,grp_led:scroll'

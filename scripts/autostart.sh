#!/bin/bash
# Startup settings and applications 
# author: syr1ken

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# Monitors
xrandr --output HDMI-0 --primary --mode 1920x1080 --rate 144.00

# Tray
run nm-applet
run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
run numlockx on

# Compositor
run picom -b --config  $HOME/.config/awesome/picom.conf

# Hide mouse when typing
run unclutter

# Screenshots (other than scrot)
run flameshot

# Wallpapers
run source $HOME/scripts/fehbg.sh

# Keyboard layout script
run source $HOME/scripts/ru_eng.sh

exec dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY &

# Zsh
export ZDOTDIR="$HOME/.config/zsh"

# Environment variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# For QT Themes
export QT_QPA_PLATFORMTHEME=qt5ct

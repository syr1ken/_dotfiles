local status_ok, lualine = pcall(require, "lualine")
if not status_ok then
    return
end

local THEME = {
    normal  = {
        a = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        b = { fg = COLORS.black, bg = COLORS.yellow, gui = "bold" },
        c = { fg = COLORS.light_gray, bg = COLORS.light_gray },
        x = { fg = COLORS.white, bg = COLORS.black },
        y = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        z = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
    },
    visual  = {
        b = { fg = COLORS.white, bg = COLORS.magenta, gui = "bold" },
        y = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        z = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
    },
    replace = {
        b = { fg = COLORS.white, bg = COLORS.red, gui = "bold" },
        y = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        z = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
    },
    insert  = {
        b = { fg = COLORS.black, bg = COLORS.green, gui = "bold" },
        y = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        z = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
    },
    command = {
        b = { fg = COLORS.white, bg = COLORS.blue, gui = "bold" },
        y = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
        z = { fg = COLORS.light_gray, bg = COLORS.black, gui = "bold" },
    },
}

local branch = {
    "branch",
    icons_enabled = true,
    icon = "",
}

local mode = {
    "mode",
    fmt = function(str)
        return " " .. str .. " "
    end,
}

local encoding = {
    "encoding",
}

local filetype = {
    "filetype",
    icons_enabled = false,
    icon = nil,
}


local location = {
    "location",
    padding = 0,
}

-- cool function for progress
local progress = function()
    local current_line = vim.fn.line(".")
    local total_lines = vim.fn.line("$")
    local chars = { "  ", "▁▁", "▂▂", "▃▃", "▄▄", "▅▅", "▆▆", "▇▇", "██" }
    local line_ratio = current_line / total_lines
    local index = math.ceil(line_ratio * #chars)
    return chars[index]
end

lualine.setup({
    options = {
        icons_enabled = true,
        theme = THEME,
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        disabled_filetypes = { "dashboard", "Outline" },
        always_divide_middle = true,
        globalstatus = true,
        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        },
    },
    sections = {
        lualine_a = { branch },
        lualine_b = { mode },
        lualine_c = {},
        lualine_x = { encoding, "filename", filetype },
        lualine_y = { location },
        lualine_z = { progress },
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { "filename" },
        lualine_x = { "location" },
        lualine_y = {},
        lualine_z = {},
    },
    tabline = {},
    extensions = {},
})

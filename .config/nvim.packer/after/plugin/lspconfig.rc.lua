local status, nvim_lsp = pcall(require, "lspconfig")
if not status then
    return
end

-- Add additional capabilities supported by nvim-cmp
local capabilities = require("cmp_nvim_lsp").default_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local util = require 'lspconfig.util'

local function get_typescript_server_path(root_dir)
    local global_ts = '/home/syr1ken/.npm/lib/node_modules/typescript/lib'

    -- local global_ts = '/usr/local/lib/node_modules/typescript/lib'
    local found_ts = ''
    local function check_dir(path)
        found_ts = util.path.join(path, 'node_modules', 'typescript', 'lib')
        if util.path.exists(found_ts) then
            return path
        end
    end
    if util.search_ancestors(root_dir, check_dir) then
        return found_ts
    else
        return global_ts
    end
end

local function lsp_highlight_document(client)
    -- Set autocommands conditional on server_capabilities
    if client.server_capabilities.document_highlight then
        vim.api.nvim_exec(
            [[
              augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
              augroup END
            ]],
            false
        )
    end
end

local function lsp_format_document(client)
    if client.server_capabilities.documentFormattingProvider then
        vim.api.nvim_command [[autogroup Format]]
        vim.api.nvim_command [[autocmd! * <buffer>]]
        vim.api.nvim_command [[autocmd BufWritePre <buffer> lua.vim.lsp.buf.format()]]
        vim.api.nvim_command [[autogroup END]]
    end
end

local function lsp_keymaps(bufnr)
    local opts = { noremap = true, silent = true }
end

local on_attach = function(client, bufnr)
    lsp_keymaps(bufnr)
    lsp_highlight_document(client)
    -- lsp_format_document(client)
end

nvim_lsp.ts_ls.setup {
    on_attach = on_attach,
    filetypes = { "typescript", "javascriptreact", "typescriptreact", "typescript.tsx", "javascript" },
    cmd = { "typescript-language-server", "--stdio" },
    init_options = {
        plugins = {
            {
                name = '@vue/typescript-plugin',
                location = vim.fn.stdpath 'data' .. '/mason/packages/vue-language-server/node_modules/@vue/language-server',
                languages = { 'vue' },
            },
        },
    },
    settings = {
        typescript = {
            tsserver = {
                useSyntaxServer = false,
            },
            inlayHints = {
                includeInlayParameterNameHints = 'all',
                includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                includeInlayFunctionParameterTypeHints = true,
                includeInlayVariableTypeHints = true,
                includeInlayVariableTypeHintsWhenTypeMatchesName = true,
                includeInlayPropertyDeclarationTypeHints = true,
                includeInlayFunctionLikeReturnTypeHints = true,
                includeInlayEnumMemberValueHints = true,
            },
        },
    },
    capabilities = capabilities,
}

nvim_lsp.lua_ls.setup {
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { 'vim' },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
}

nvim_lsp.volar.setup {
    on_attach = on_attach,
    -- on_new_config = function(new_config, new_root_dir)
    --     new_config.init_options.typescript.tsdk = get_typescript_server_path(new_root_dir)
    -- end,
    on_new_config = function(new_config, new_root_dir)
        local lib_path = vim.fs.find('node_modules/typescript/lib', { path = new_root_dir, upward = true })[1]
        if lib_path then
            new_config.init_options.typescript.tsdk = lib_path
        end
    end,
    filetypes = { 'vue' },
    settings = {
        typescript = {
            inlayHints = {
                enumMemberValues = {
                    enabled = true,
                },
                functionLikeReturnTypes = {
                    enabled = true,
                },
                propertyDeclarationTypes = {
                    enabled = true,
                },
                parameterTypes = {
                    enabled = true,
                    suppressWhenArgumentMatchesName = true,
                },
                variableTypes = {
                    enabled = true,
                },
            },
        },
    },
    init_options = {
        vue = {
            hybridMode = false,
        },
        typescript = {
            tsdk = '/usr/lib/node_modules/typescript/lib'
        }
    },
    cmd = { "vue-language-server", "--stdio" },
    capabilities = capabilities,
}

-- nvim_lsp.ccls.setup {
--     on_attach = on_attach,
--     on_new_config = function(new_config, new_root_dir)
--         new_config.init_options.typescript.tsdk = get_typescript_server_path(new_root_dir)
--     end,
--     filetypes = { "c", "cpp", "objc", "objcpp" },
--     cmd = { "ccls" },
--     capabilities = capabilities,
--     init_options = {
--         compilationDatabaseDirectory = "build",
--         index = {
--             threads = 0,
--         },
--         clang = {
--             excludeArgs = { "-frounding-math" },
--         },
--     }
-- }

nvim_lsp.clangd.setup {
    on_attach = on_attach,
    filetypes = { "markdown" },
    cmd = { "marksman", "server" },
    capabilities = capabilities,
}

nvim_lsp.cssls.setup {
    on_attach = on_attach,
    filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "proto" },
    settings = {
        {
            css = {
                validate = true
            },
            less = {
                validate = true
            },
            scss = {
                validate = true
            }
        }
    },
    cmd = { "clangd" },
    capabilities = capabilities,
}

nvim_lsp.html.setup {
    on_attach = on_attach,
    filetypes = { "html" },
    init_options = {
        configurationSection = { "html", "css", "javascript" },
        embeddedLanguages = {
            css = true,
            javascript = true
        },
        provideFormatter = true
    },
    cmd = { "vscode-html-language-server", "--stdio" },
    capabilities = capabilities,
}

nvim_lsp.tailwindcss.setup {
    on_attach = on_attach,
    cmd = { "tailwindcss-language-server", "--stdio" },
    filetypes = { "html", "css", "scss", "javascript", "javascriptreact", "typescript", "typescriptreact", "vue" },
    root_dir = nvim_lsp.util.root_pattern("tailwind.config.js", "tailwind.config.ts", "package.json"),
    capabilities = capabilities,
}

nvim_lsp.jsonls.setup {
    on_attach = on_attach,
    filetypes = { "json", "jsonc" },
    init_options = {
        provideFormatter = true
    },
    cmd = { "vscode-json-language-server", "--stdio" },
    capabilities = capabilities,
}

nvim_lsp.marksman.setup {
    on_attach = on_attach,
    filetypes = { "markdown" },
    cmd = { "marksman", "server" },
    capabilities = capabilities,
}

-- nvim_lsp.pasls.setup {
--     on_attach = on_attach,
--     filetypes = { "pascal" },
--     cmd = { "pasls" },
--     capabilities = capabilities,
-- }

nvim_lsp.ccls.setup {
    on_attach = on_attach,
    filetypes = { "css", "scss", "less" },
    cmd = { "vscode-css-language-server", "--stdio" },
    settings = {
        css = {
            validate = true
        },
        less = {
            validate = true
        },
        scss = {
            validate = true
        }
    },
    init_options = { provideFormatter = true }, -- needed to enable formatting capabilities
    root_dir = util.root_pattern('package.json', '.git'),
    capabilities = capabilities,
}

local M = {}

function M.setup(opts, keymap)
    keymap("n", "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<cr>", opts)
    keymap("n", "<leader>lf", "<cmd>lua vim.lsp.buf.format()<cr>", opts)
    keymap("n", "<leader>li", "<cmd>LspInfo<cr>", opts)
    keymap("n", "<leader>lI", "<cmd>LspInstallInfo<cr>", opts)
    keymap("n", "gf", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
    keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
    keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
end

return M

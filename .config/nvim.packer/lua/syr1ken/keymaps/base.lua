local M = {}

function M.setup(opts, keymap)
    -- Remap space as leader key
    keymap("", "<Space>", "<Nop>", opts)
    vim.g.mapleader = " "
    vim.g.maplocalleader = " "

    -- Modes
    --   normal_mode = "n",
    --   insert_mode = "i",
    --   visual_mode = "v",
    --   visual_block_mode = "x",
    --   term_mode = "t",
    --   command_mode = "c",

    -- Normal --
    -- Better window navigation
    keymap("n", "sk", "<C-w>k", opts)
    keymap("n", "sj", "<C-w>j", opts)
    keymap("n", "sh", "<C-w>h", opts)
    keymap("n", "sl", "<C-w>l", opts)

    -- Navigate buffers
    keymap("n", "<S-l>", ":bnext<CR>", opts)
    keymap("n", "<S-h>", ":bprevious<CR>", opts)

    -- Visual --
    -- Stay in indent mode
    keymap("v", "<", "<gv", opts)
    keymap("v", ">", ">gv", opts)
    keymap("v", "p", '"_dP', opts)

    -- Visual Block --
    -- Move text up and down
    keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
    keymap("x", "K", ":move '<-2<CR>gv-gv", opts)

    -- Comment
    keymap("n", "<leader>/", "<Plug>(comment_toggle_linewise_current)", opts)
    keymap("v", "<leader>/", "<Plug>(comment_toggle_linewise_visual)", opts)
end

return M

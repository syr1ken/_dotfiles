local M = {}

function M.setup(opts, keymap)
    keymap(
        "n",
        "<leader>ff",
        "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
        opts
    )
    keymap("n", "<leader>fg", "<cmd>Telescope live_grep<cr>", opts)
    keymap("n", "<leader>fb", "<cmd>Telescope buffers<cr>", opts)
    keymap("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", opts)
    keymap("n", "<leader>fm", "<cmd>Telescope man_pages<cr>", opts)
    keymap("n", "<leader>fk", "<cmd>Telescope keymaps<cr>", opts)
    keymap("n", "<leader>fc", "<cmd>Telescope commands<cr>", opts)
end

return M

vim.opt.cursorline = true
vim.opt.termguicolors = true
vim.opt.winblend = 10
vim.opt.wildoptions = 'pum'
vim.opt.pumblend = 10
vim.opt.background = 'dark'

-- Set VIM colorscheme
vim.cmd(":colorscheme vim")

-- Highlight yanked text for 200ms using the "Visual" highlight group
vim.cmd [[
  augroup highlight_yank
  autocmd!
  au TextYankPost * silent! lua vim.highlight.on_yank({higroup="Visual", timeout=100})
  augroup END
]]

-- Change standard NVIM Groups Highlights to blue
vim.cmd("highlight NormalFloat ctermfg=White ctermbg=DarkBlue guifg='" ..
COLORS.white .. "' guibg='" .. COLORS.dark_blue .. "'")
vim.cmd("highlight Pmenu ctermfg=White ctermbg=DarkBlue guifg='" ..
COLORS.white .. "' guibg='" .. COLORS.dark_blue .. "'")
vim.cmd("highlight NonText ctermfg=Grey guifg='" .. COLORS.grey .. "'")

-- Gitsigns Highlights
-- Add
vim.cmd("highlight DiffAdd ctermfg=Blue ctermbg=Blue guifg='" .. COLORS.Blue .. "' guibg='" .. COLORS.Blue .. "'")
vim.cmd("highlight GitSignsAdd ctermfg=Blue ctermbg=Blue guifg='" .. COLORS.Blue .. "' guibg='" .. COLORS.Blue .. "'")
vim.cmd("highlight GitSignsAddPreview ctermfg=Black ctermbg=Black guifg='" ..
COLORS.White .. "' guibg='" .. COLORS.Normal .. "'")
vim.cmd("highlight GitSignsAddLn ctermfg=Blue ctermbg=Blue guifg='" .. COLORS.Blue .. "' guibg='" .. COLORS.Blue .. "'")

-- Untracked
vim.cmd("highlight GitSignsUntracked ctermfg=Green ctermbg=Green guifg='" ..
COLORS.Green .. "' guibg='" .. COLORS.Green .. "'")
vim.cmd("highlight GitSignsUntrackedLn ctermfg=Green ctermbg=Green guifg='" ..
COLORS.Green .. "' guibg='" .. COLORS.Green .. "'")

-- Changed
vim.cmd("highlight DiffChange ctermfg=Magenta ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChange ctermfg=Magenta ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChangeNr ctermfg=Magenta ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChangeLn ctermfg=Black ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChangedelete ctermfg=Black ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChangedeleteNr ctermfg=Black ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")
vim.cmd("highlight GitSignsChangedeleteLn ctermfg=Black ctermbg=Magenta guifg='" ..
COLORS.Magenta .. "' guibg='" .. COLORS.Magenta .. "'")

-- Delete
vim.cmd("highlight DiffDelete ctermfg=Red ctermbg=Red guifg='" .. COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsDelete ctermfg=Red ctermbg=Red guifg='" .. COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsDeleteNr ctermfg=Red ctermbg=Red guifg='" .. COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsTopDelete ctermfg=Red ctermbg=Red guifg='" .. COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsTopDeleteNr ctermfg=Red ctermbg=Red guifg='" .. COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsDeleteVirtLn ctermfg=Red ctermbg=Red guifg='" ..
COLORS.Red .. "' guibg='" .. COLORS.Red .. "'")
vim.cmd("highlight GitSignsDeletePreview ctermfg=White ctermbg=Black guifg='" ..
COLORS.White .. "' guibg='" .. COLORS.Normal .. "'")

-- Telescope
vim.cmd("highlight TelescopeNormal ctermfg=White ctermbg=DarkBlue guifg='" .. COLORS.white .. "' guibg='" .. COLORS.dark_blue .. "'")

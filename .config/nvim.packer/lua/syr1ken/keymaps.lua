local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

-- Main keymaps
local main = require("syr1ken.keymaps.base")
main.setup(opts, keymap)

-- LSP
local lsp = require("syr1ken.keymaps.lsp")
lsp.setup(opts, keymap)

local telescope = require("syr1ken.keymaps.telescope")
telescope.setup(opts, keymap)

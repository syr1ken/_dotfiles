return {
    "lukas-reineke/indent-blankline.nvim",
    config = function()
        local status_ok, ibl = pcall(require, "ibl")
        if not status_ok then
            return
        end

        vim.g.indent_blankline_char = "▏"
        vim.opt.termguicolors = true
        vim.opt.list = true
        vim.opt.listchars:append "space:⋅"

        local highlight = {
            "RainbowRed",
            "RainbowYellow",
            "RainbowBlue",
            "RainbowOrange",
            "RainbowGreen",
            "RainbowViolet",
            "RainbowCyan",
        }

        local hooks = require "ibl.hooks"
        -- create the highlight groups in the highlight setup hook, so they are reset
        -- every time the colorscheme changes
        hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
            vim.api.nvim_set_hl(0, "RainbowRed", { fg = COLORS.red })
            vim.api.nvim_set_hl(0, "RainbowYellow", { fg = COLORS.yellow })
            vim.api.nvim_set_hl(0, "RainbowBlue", { fg = COLORS.blue })
            vim.api.nvim_set_hl(0, "RainbowOrange", { fg = COLORS.orange })
            vim.api.nvim_set_hl(0, "RainbowGreen", { fg = COLORS.green })
            vim.api.nvim_set_hl(0, "RainbowViolet", { fg = COLORS.purple })
            vim.api.nvim_set_hl(0, "RainbowCyan", { fg = COLORS.cyan })
        end)

        ibl.setup { indent = { highlight = highlight } }
      end,
}

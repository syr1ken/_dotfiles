return {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = function()
        local status, autopairs = pcall(require, "nvim-autopairs")

        autopairs.setup({
            disable_filetype = { "TelescopePrompt", "vim" },
        })
    end,
}

local opts = { noremap = true, silent = true }

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Better window navigation
vim.keymap.set("n", "sk", "<C-w>k", opts);
vim.keymap.set("n", "sj", "<C-w>j", opts);
vim.keymap.set("n", "sh", "<C-w>h", opts);
vim.keymap.set("n", "sl", "<C-w>l", opts);

-- Navigate buffers
vim.keymap.set("n", "<S-l>", ":bnext<CR>", opts);
vim.keymap.set("n", "<S-h>", ":bprevious<CR>", opts);


-- Stay in indent mode
vim.keymap.set("v", "<", "<gv", opts);
vim.keymap.set("v", ">", ">gv", opts);
vim.keymap.set("v", "p", '"_dP', opts);

-- Move text up and down
vim.keymap.set("x", "J", ":move '>+1<CR>gv-gv", opts);
vim.keymap.set("x", "K", ":move '<-2<CR>gv-gv", opts);

-- Comment
vim.keymap.set("n", "<leader>/", "<Plug>(comment_toggle_linewise_current)", opts);
vim.keymap.set("v", "<leader>/", "<Plug>(comment_toggle_linewise_visual)", opts);

-- Telescope
vim.keymap.set("n",
    "<leader>ff",
    "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
    opts);

vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<cr>", opts);
vim.keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>", opts);
vim.keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", opts);
vim.keymap.set("n", "<leader>fm", "<cmd>Telescope man_pages<cr>", opts);
vim.keymap.set("n", "<leader>fk", "<cmd>Telescope keymaps<cr>", opts);
vim.keymap.set("n", "<leader>fc", "<cmd>Telescope commands<cr>", opts);

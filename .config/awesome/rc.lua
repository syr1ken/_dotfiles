--[[

#                 _ _
#  ___ _   _ _ __/ | | _____ _ __
# / __| | | | '__| | |/ / _ \ '_ \
# \__ \ |_| | |  | |   <  __/ | | |
# |___/\__, |_|  |_|_|\_\___|_| |_|
#      |___/
#
#  My Awesome WM Config.

# https://awesomewm.org/doc/api/documentation/05-awesomerc.md.html
--]]
--
-- Colors
local colors = require("themes.syr1ken.colors")

local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

-- Standard awesome library
local gears = require("gears") --Utilities such as color parsing and objects
local awful = require("awful") --Everything related to window managment
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")
naughty.config.defaults["icon_size"] = 100

local lain = require("lain")
local freedesktop = require("freedesktop")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
local hotkeys_popup = require("awful.hotkeys_popup").widget
local hotkeys = require("awful.hotkeys_popup.keys")
hotkeys.tmux.add_rules_for_terminal({ rule = { name = "no window ever has a name like this" }});
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi = require("beautiful.xresources").apply_dpi

-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors,
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        if in_error then
            return
        end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err),
        })
        in_error = false
    end)
end

-- keep themes in alfabetical order for ATT
local themes = {
    "syr1ken",
}

-- choose your theme here
local chosen_theme = themes[1]
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)

-- modkey or mod4 = super key
local modkey = "Mod4"
local modkey1 = "Control"

-- personal variables
--change these variables if you want
local browser1 = "firefox"
local browser2 = "brave"
local editor = os.getenv("EDITOR") or "vim"
local editorgui = "emacsclient -c -a 'emacs' "
local filemanager = "yazi"
local terminal = "alacritty"

-- awesome variables
awful.util.terminal = terminal
awful.util.tagnames = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }

awful.layout.suit.tile.left.mirror = true
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
}

awful.util.taglist_buttons = my_table.join(
    awful.button({}, 1, function(t)
        t:view_only()
    end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t)
        awful.tag.viewnext(t.screen)
    end),
    awful.button({}, 5, function(t)
        awful.tag.viewprev(t.screen)
    end)
)

awful.util.tasklist_buttons = my_table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            -- Without this, the following
            -- :isvisible() makes no sense
            c.minimized = false
            if not c:isvisible() and c.first_tag then
                c.first_tag:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
        end
    end),
    awful.button({}, 3, function()
        local instance = nil

        return function()
            if instance and instance.wibox.visible then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({ theme = { width = dpi(250) } })
            end
        end
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end)
)

lain.layout.termfair.nmaster = 3
lain.layout.termfair.ncol = 1
lain.layout.termfair.center.nmaster = 3
lain.layout.termfair.center.ncol = 1
lain.layout.cascade.tile.offset_x = dpi(2)
lain.layout.cascade.tile.offset_y = dpi(32)
lain.layout.cascade.tile.extra_padding = dpi(5)
lain.layout.cascade.tile.nmaster = 5
lain.layout.cascade.tile.ncol = 2

awful.util.mymainmenu = freedesktop.menu.build({
    after = {
        { "Terminal", terminal },
        {
            "Log out",
            function()
                awesome.quit()
            end,
        },
        { "Sleep",    "systemctl suspend" },
        { "Restart",  "systemctl reboot" },
        { "Shutdown", "systemctl poweroff" },
        -- other triads can be put here
    },
})

screen.connect_signal("property::geometry", function(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end)

-- No borders when rearranging only 1 non-floating or maximized client
-- screen.connect_signal("arrange", function(s)
--     local only_one = #s.tiled_clients == 1
--     for _, c in pairs(s.clients) do
--         if only_one and not c.floating or c.maximized then
--             c.border_width = 2
--         else
--             c.border_width = beautiful.border_width
--         end
--     end
-- end)

-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s)
    beautiful.at_screen_connect(s)
    s.systray = wibox.widget.systray()
    s.systray.visible = true
end)

-- Mouse bindings
root.buttons(my_table.join(
    awful.button({}, 3, function()
        awful.util.mymainmenu:toggle()
    end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))

-- Key bindings
globalkeys = my_table.join(
-- Browsers
    awful.key({ modkey }, "w", function()
        awful.util.spawn(browser1)
    end, { description = browser1, group = "super" }),

    awful.key({ modkey, "Shift" }, "w", function()
        awful.util.spawn(browser2)
    end, { description = browser2, group = "super" }),

    awful.key({ modkey, "Shift" }, "Return", function()
        awful.util.spawn("dmenu_run -f -h 23 -fn 'Source Code Pro Black' " ..
            "-nb '" ..
            -- Bar background color
            colors.dark_blue ..
            "' -nf '" ..
            -- Bar background text color
            colors.white ..
            "' -sb '" ..
            -- Selected background color
            colors.white ..
            "' -sf '" ..
            -- Selected text color
            colors.black ..
            "' -nhb '" ..
            -- None selected background for fzf
            colors.dark_blue ..
            "' -nhf '" ..
            -- None selected text color for fzf
            colors.red ..
            "' -shb '" ..
            -- Selected background color for fzf
            colors.white ..
            "' -shf '" ..
            -- Selected text color for fzf
            colors.red .. "'")
    end, { description = "show dmenu", group = "Super" }),

    -- super + ...
    awful.key({ modkey }, "e", function()
        awful.util.spawn(editorgui)
    end, { description = "run gui editor", group = "super" }),

    awful.key({ modkey }, "v", function()
        awful.util.spawn("pavucontrol")
    end, { description = "pulseaudio control", group = "super" }),

    awful.key({ modkey, "Shift" }, "q", function()
        awful.util.spawn("xkill")
    end, { description = "Kill proces", group = "super" }),

    awful.key({ modkey }, "o", function()
        awful.spawn.with_shell("$HOME/.config/awesome/scripts/picom-toggle.sh")
    end, { description = "Picom toggle", group = "super" }),

    awful.key({ modkey }, "Return", function()
        awful.spawn(terminal)
    end, { description = terminal, group = "super" }),

    awful.key({ modkey, modkey1 }, "r", awesome.restart, { description = "reload awesome", group = "super" }),

    awful.key({ modkey, modkey1 }, "Return", function()
        awful.util.spawn(filemanager)
    end, { description = "file manager", group = "super" }),

    -- screenshots
    awful.key({ modkey }, "s", function()
        awful.util.spawn("flameshot gui")
    end, { description = "flameshot", group = "screenshots" }),

    -- Hotkeys Awesome
    awful.key({ modkey }, "d", hotkeys_popup.show_help, { description = "show help", group = "help" }),

    -- By direction client focus
    awful.key({ modkey }, "j", function()
        awful.client.focus.global_bydirection("down")
        if client.focus then
            client.focus:raise()
        end
    end, { description = "focus down", group = "client" }),

    awful.key({ modkey }, "k", function()
        awful.client.focus.global_bydirection("up")
        if client.focus then
            client.focus:raise()
        end
    end, { description = "focus up", group = "client" }),

    awful.key({ modkey }, "h", function()
        awful.client.focus.global_bydirection("left")
        if client.focus then
            client.focus:raise()
        end
    end, { description = "focus left", group = "client" }),

    awful.key({ modkey }, "l", function()
        awful.client.focus.global_bydirection("right")
        if client.focus then
            client.focus:raise()
        end
    end, { description = "focus right", group = "client" }),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "j", function()
        awful.client.swap.byidx(1)
    end, { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function()
        awful.client.swap.byidx(-1)
    end, { description = "swap with previous client by index", group = "client" }),

    -- Show/Hide Wibox
    awful.key({ modkey }, "b", function()
        for s in screen do
            s.mywibox.visible = not s.mywibox.visible
            if s.mybottomwibox then
                s.mybottomwibox.visible = not s.mybottomwibox.visible
            end
        end
    end, { description = "toggle wibox", group = "awesome" }),

    -- Show/Hide Systray
    awful.key({ modkey }, "t", function()
        awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible
    end, { description = "Toggle systray visibility", group = "awesome" }),

    awful.key({ modkey }, "Tab", function()
        awful.layout.inc(1)
    end, { description = "select next", group = "layout" })
)

clientkeys = my_table.join(
    awful.key({ modkey }, "f", function(c)
        c.fullscreen = not c.fullscreen
        c:raise()
    end, { description = "toggle fullscreen", group = "client" }),

    awful.key({ modkey }, "q", function(c)
        c:kill()
    end, { description = "close", group = "hotkeys" }),

    awful.key(
        { modkey, "Shift" },
        "f",
        awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }
    ),

    awful.key({ modkey }, "n", function(c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
    end, { description = "minimize", group = "client" }),

    awful.key({ modkey }, "m", function(c)
        c.maximized = not c.maximized
        c:raise()
    end, { description = "maximize", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = { description = "view tag #", group = "tag" }
        descr_toggle = { description = "toggle tag #", group = "tag" }
        descr_move = { description = "move focused client to tag #", group = "tag" }
        descr_toggle_focus = { description = "toggle focused client on tag #", group = "tag" }
    end
    globalkeys = my_table.join(
        globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
                tag:view_only()
            end
        end, descr_view),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end, descr_toggle),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then
                    client.focus:move_to_tag(tag)
                    tag:view_only()
                end
            end
        end, descr_move),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end, descr_toggle_focus)
    )
end

clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),

    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),

    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen,
            size_hints_honor = false,
        },
    },

    -- Titlebars
    {
        rule_any = {
            type = { "dialog", "normal" }
        },
        properties = { titlebars_enabled = false }
    },

    -- Set applications to be maximized at startup.
    -- find class or role via xprop command
    {
        rule = { class = editorgui },
        properties = { maximized = true }
    },

    {
        rule = { class = "Gimp*", role = "gimp-image-window" },
        properties = { maximized = true }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",   -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
            },
            class = {
                "Galculator",
                "MessageWin", -- kalarm.
            },

            name = {
                "Event Tester", -- xev.
                "Friends List"
            },
            role = {
                "pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
                "Preferences",
                "setup",
            },
        },
        properties = { floating = true },
    },

    -- Floating clients but centered in screen
    {
        rule_any = {
            class = {
                "Polkit-gnome-authentication-agent-1",
            },
        },
        properties = { floating = true },
        callback = function(c)
            awful.placement.centered(c, nil)
        end,
    },
}

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
end)

client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
end)

-- Autostart applications
awful.spawn.with_shell("~/scripts/autostart.sh")

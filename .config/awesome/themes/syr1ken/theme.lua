--[[

#                 _ _
#  ___ _   _ _ __/ | | _____ _ __
# / __| | | | '__| | |/ / _ \ '_ \
# \__ \ |_| | |  | |   <  __/ | | |
# |___/\__, |_|  |_|_|\_\___|_| |_|
#      |___/
#
#  My Awesome WM Theme.

--]]

local gears = require("gears")
local lain = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi = require("beautiful.xresources").apply_dpi

local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

-- Theme
local theme = {}

-- Colors
local colors = require("themes.syr1ken.colors")

-- Fonts
local fonts = require("themes.syr1ken.fonts")

theme.confdir = os.getenv("HOME") .. "/.config/awesome/themes/syr1ken"
theme.font = fonts.main_font_bold

-- Awesome bars
theme.bg_normal = colors.black
theme.bg_focus = colors.black
theme.bg_urgent = colors.black

theme.fg_normal = colors.white
theme.fg_focus = colors.orange
theme.fg_urgent = colors.red
theme.fg_minimize = colors.white

-- Border
theme.border_width = dpi(2)
theme.border_normal = colors.arc_dark_blue
theme.border_focus = colors.orange
theme.border_marked = colors.arc_dark_blue

-- Menu
theme.menu_bg_normal = colors.black
theme.menu_bg_focus = colors.black
theme.menu_border_width = 0
theme.menu_height = dpi(25)
theme.menu_width = dpi(260)
theme.menu_submenu_icon = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal = colors.white
theme.menu_fg_focus = colors.white
theme.menu_bg_normal = colors.black
theme.menu_bg_focus = colors.orange

theme.widget_mail = theme.confdir .. "/icons/mail.png"
theme.widget_music = theme.confdir .. "/icons/note.png"
theme.widget_music_on = theme.confdir .. "/icons/note.png"
theme.widget_music_pause = theme.confdir .. "/icons/pause.png"
theme.widget_music_stop = theme.confdir .. "/icons/stop.png"

-- Taglist
theme.taglist_squares_sel = theme.confdir .. "/icons/square_sel.png"
theme.taglist_squares_unsel = theme.confdir .. "/icons/square_unsel.png"
theme.taglist_disable_icon = false

theme.tasklist_plain_task_name = true
theme.tasklist_disable_icon = true
theme.useless_gap = 5
theme.layout_tile = theme.confdir .. "/icons/tile.png"
theme.layout_tilegaps = theme.confdir .. "/icons/tilegaps.png"
theme.layout_tileleft = theme.confdir .. "/icons/tileleft.png"
theme.layout_tilebottom = theme.confdir .. "/icons/tilebottom.png"
theme.layout_tiletop = theme.confdir .. "/icons/tiletop.png"
theme.layout_fairv = theme.confdir .. "/icons/fairv.png"
theme.layout_fairh = theme.confdir .. "/icons/fairh.png"
theme.layout_spiral = theme.confdir .. "/icons/spiral.png"
theme.layout_dwindle = theme.confdir .. "/icons/dwindle.png"
theme.layout_max = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating = theme.confdir .. "/icons/floating.png"
theme.titlebar_close_button_normal = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"

-- Widgets
local markup = lain.util.markup

-- Net
theme.widget_netdown = theme.confdir .. "/icons/net_down.png"
theme.widget_netup = theme.confdir .. "/icons/net_up.png"

local netdownicon = wibox.widget.imagebox(theme.widget_netdown)
local netdowninfo = wibox.widget.textbox()
local netupicon = wibox.widget.imagebox(theme.widget_netup)
local netupinfo = lain.widget.net({
	timeout = 1,
	settings = function()
		widget:set_markup(markup.fontfg(theme.font, colors.red, net_now.sent .. " "))
		netdowninfo:set_markup(markup.fontfg(theme.font, colors.green, net_now.received .. " "))
	end,
})

-- MEM
theme.widget_mem = theme.confdir .. "/icons/mem.png"

local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
	timeout = 1,
	settings = function()
		widget:set_markup(markup.fontfg(theme.font, colors.yellow, mem_now.used .. "M "))
	end,
})

-- CPU
theme.widget_cpu = theme.confdir .. "/icons/cpu.png"

local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
	timeout = 1,
	settings = function()
		widget:set_markup(markup.fontfg(theme.font, colors.orange, cpu_now.usage .. "% "))
	end,
})

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
theme.widget_clock = theme.confdir .. "/icons/clock.png"
-- local clockicon = wibox.widget.imagebox(theme.widget_clock)

local mydate = wibox.widget.textclock(markup(colors.white, "%a %d %b ") .. markup(colors.white, "%H:%M"))
mydate.font = theme.font
mydate.refresh = 1

-- Calendar
theme.cal = lain.widget.cal({
	attach_to = { mydate },
	notification_preset = {
		font = theme.font,
		fg = theme.fg_normal,
		bg = theme.bg_normal,
	},
})

-- Keyboard layout
theme.keyboard_layout = theme.confdir .. "/icons/keyboard.png"

-- local keyboardicon = wibox.widget.imagebox(theme.keyboard_layout)
local keyboardlayout = awful.widget.keyboardlayout:new()

-- Systray
local systray = wibox.widget.systray()
systray.forced_height = 2

-- Separators
local spr = wibox.widget.textbox(" ")

function theme.at_screen_connect(s)
	-- If wallpaper is a function, call it with the screen
	local wallpaper = theme.wallpaper
	if type(wallpaper) == "function" then
		wallpaper = wallpaper(s)
	end
	gears.wallpaper.maximized(wallpaper, s, true)

	-- Tags
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	-- Create an imagebox widget which will contains an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(my_table.join(
		awful.button({}, 1, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 3, function()
			awful.layout.inc(-1)
		end),
		awful.button({}, 4, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 5, function()
			awful.layout.inc(-1)
		end)
	))
	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

	-- Create the wibox
	s.mywibox =
		awful.wibar({ position = "top", screen = s, height = dpi(23), bg = theme.bg_normal, fg = theme.fg_normal })

	-- Add widgets to the wibox
	s.mywibox:setup({
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			--s.mylayoutbox,
			s.mytaglist,
			s.mypromptbox,
		},
		{
			layout = wibox.layout.fixed.horizontal,
			s.mytasklist, -- Middle widget
        },
		{ -- Right widgets
			layout = wibox.layout.fixed.horizontal,
			netdownicon,
			netdowninfo,
			netupicon,
			netupinfo.widget,
			memicon,
			memory.widget,
			cpuicon,
			cpu.widget,
			mydate,
            spr,
			keyboardlayout,
            spr,
			systray,
			s.mylayoutbox,
		},
	})
end

return theme

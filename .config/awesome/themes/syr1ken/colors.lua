local M = {}

M.arc_dark_gloom_gray = "#404552"
M.arc_dark_dark_gray = "#383c4a"
M.arc_dark_gray = "#4b5162"
M.arc_dark_light_gray = "#7c818c"
M.arc_dark_blue = "#05f5f4"
M.red = "#f60302"
M.dark_blue = "#050386"
M.green = "#5ff55d"
M.white = "#ffffff"
M.black = "#2b2b35"
M.yellow = "#f6f55d"
M.orange = "#f7a002"
M.magenta = "#f103f4"

return M

local M = {}

M.main_font = "Source Code Pro Bold 12"
M.main_font_bold = "Source Code Pro Bold 12"

return M
